<x-layout>

 




    <div>
        @if (session('success'))

        <div class="alert alert-success">
            {{(session('success'))}}
        </div>
            
        @endif
    </div>



  {{-- sezione icone categorie  --}}

  <section class="container-fluid my-5">

    <div class="row">
      <div class="col-12">
        <h2 class="text-center mb-4">Le Nostre Categorie</h2>
      </div>
      <div class="col-12 d-flex justify-content-evenly m-2 text-center">
  @foreach ($categories as $category)
    


    <div><ul class="list"> <li><a href="{{route('announcementsFilterByCategory', compact('category'))}}"><i class="fad fa-tshirt fa-3x"></i></li> <li>{{$category->name}}</a></li></div>


  @endforeach
        {{-- <div><ul class="list"> <li><a href="{{route('announcementsFilterByCategory', $announcement->category)}}"><i class="fad fa-box-full fa-3x"></i></li> <li>Collezionismo</a> </li></div>
        <div><ul class="list"> <li><a href="{{route('announcementsFilterByCategory', $announcement->category)}}"><i class="fad fa-tv-music fa-3x"></i></li> <li>Elettronica</a></li> </div>
        <div><ul class="list"> <li><a href="{{route('announcementsFilterByCategory', $announcement->category)}}"><i class="fad fa-hand-holding-seedling fa-3x"></i></li> <li>Giardinaggio e Fai da Te</a></li> </div>
        <div><ul class="list"> <li><a href="{{route('announcementsFilterByCategory', $announcement->category)}}"><i class="fad fa-home fa-3x"></i></li> <li>Immobili</a></li></div> --}}
        
      </div>
    </div>

    {{-- <div class="row">
      <div class="col-12 d-flex justify-content-evenly m-2 text-center">
@foreach ($announcements as $announcement)
        <div> <ul class="list"> <li> <a href="{{route('announcementsFilterByCategory', $announcement->category)}}"><i class="fad fa-user-hard-hat fa-3x"></i> </li> <li>{{$announcement->category->name}}</a> </li></div>
        <div><a href=""> <ul class="list"> <li><i class="fad fa-books fa-3x"></i></li> <li>Libri</a> </li> </div>
        <div><a href=""> <ul class="list"> <li><i class="fad fa-cars fa-3x"></i></li> <li>Motori</a>  </li></div>
        <div><a href="">  <ul class="list"> <li><i class="fad fa-guitars fa-3x"></i></li> <li>Musica e Strumenti Musicali</a> </li> </div>
        <div><a href=""> <ul class="list"> <li><i class="fad fa-volleyball-ball fa-3x"></i></li> <li>Sport</a>  </li></div>
          @endforeach

      </div>
    </div> --}}


  </section>








    <section class="container-fluid bg-main">
      <div class="row">
        
      
      </div>

      <div class="row">
        <div class="col-12 d-flex justify-content-evenly">


        

              @foreach ($announcements as $announcement)

                  {{-- mostra gli ultimi 5 annunci dall' ultimo pubblicato  --}}
              
                  <div class="card mx-2 card-custom text-sec mt-5">
                    <div class="card-body">
                        <h3 class="card-title text-center text-color">{{$announcement->title}}</h3>
                        <h5 class="card-title fs-1 text-sec text-center">{{$announcement->price}} €</h5>
                    </div>
                    <ul class="list-group list-group-flush border-top border">
                        
                        <li class="list-group-item border"><img src="https://via.placeholder.com/300" class="card-img-top img-fluid"   alt="..."></li>
                        <li class="list-group-item text-center fs-5 text-color">CATEGORIA: {{$announcement->category->name}}</li>
                        <li class="list-group-item border text-center fs-6 text-color"><h5>Annuncio:</h5><p class="card-text">{{$announcement->body}}</p></li>
                        <li class="list-group-item text-center fs-5 text-color"><h5>Inserito da:</h5> <strong>{{$announcement->user->name}} </strong> , {{$announcement->created_at->format('d/m/Y')}} </li>
                    
                            
                            
                        </li>
                    </ul>
                    <div class="card-body d-flex justify-content-center">
                        <a href="{{route('announcement.show', compact('announcement'))}}" class="card-link btn btn-custom btn-lg btn-primary">Vai</a>
                        
                    </div>
                </div>

              @endforeach

          </div>


        </div>
      </div>
    </section>

  
    
   





</x-layout>