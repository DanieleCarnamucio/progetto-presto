<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Scout\Searchable;

class Announcement extends Model
{
    use HasFactory;
    use Searchable;


    protected $fillable=[
        
        'title',
        'body',
        'price',
        'category_id',
        'user_id',
       
    ];

    public function toSearchableArray()
    {

        

        $array = [

            'id' => $this->id,
            'title' => $this->title,
            'body'=> $this->body
            
            
        ];

        return $array;
    }

    public function category(){
       return $this->belongsTo(Category::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
