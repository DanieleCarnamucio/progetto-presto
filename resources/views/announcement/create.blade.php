<x-layout>
   

      {{-- form inserimento annuncio  --}}
    
        <section class="container-fluid">
          <iv class="row">
            <div class="col-12">

              <form method="POST" action="{{route('announcement.store')}}">
                @csrf
                
                <h1 class="h3 mb-3 fw-normal">Inserisci il tuo annuncio</h1>
    
                <div class="form-floating">
                  <input name="title" type="text" class="form-control" id="ftitle" placeholder="Nome" @error('name') is-invalid @enderror" required>
                  <label for="ftitle">Titolo</label>
                </div>
    
                <div class="form-floating">
                  <input name="price" type="number" class="form-control" id="fprice" placeholder="Price" required>
                  <label for="fprice">Prezzo</label>
                </div>
            
                <div class="form-floating">
                  <select name="category" class="w-100 " id="fcategory" required>
                    <option>Seleziona una categoria per il tuo annuncio</option>
    
                    
                    @foreach ($categories as $category)
                    
                    <option value="{{$category->id}}">{{$category->name}}</option>
    
                        
                    @endforeach
    
                    
                    
                  </select>
                  <label for="ftitle">Categoria</label>
                </div>
    
                <div class="form-floating">
                    <textarea name="body"  class="form-control" id="fbody">
                       Inserisci la descrizione del tuo annuncio
                    </textarea>
                    <label for="fbody">Descrizione</label>
                </div>
               
            
              
                <button class="w-100 btn btn-lg btn-custom" type="submit">Aggiungi nuovo Annuncio</button>
                
              </form>

            </div>
          </div>
        </section>
        
       
        
        
            
         
</x-layout>