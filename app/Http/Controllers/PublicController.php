<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PublicController extends Controller
{
    public function index(){

        $categories = Category::all(); 

       
        $announcements = Announcement::orderBy('created_at', 'DESC')->take(5)->get();
        return view('home', compact('announcements', 'categories'));
    }

    public function search(Request $request){

        $q = $request->input('q');

        $announcements = Announcement::search($q)->get();

        return view('search_results', compact('q', 'announcements'));

    }



    public function announcementsFilterByCategoryF(Category $category,){

      
        // $category=Category::find($category_id);
        $announcements= $category->announcements()->paginate(5);
        
        // $category= DB::table('announcements')->where('category_id', $category->id)->get();
        // dd($category);

        return view('announcementsFilterByCategory', compact('category','announcements'));
    }
    
}
