<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\AnnouncementController;
use App\Models\Category;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',[PublicController::class,'index'])->name('home');

//ricerca
Route::get('/search', [PublicController::class, 'search'])->name('search');

//Annunci
Route::get('/announcement/index',[AnnouncementController::class,'index'])->name('announcement.index');
Route::get('/announcement/create',[AnnouncementController::class,'create'])->name('announcement.create');
// Route::get('/announcement/show/{announcement}',[AnnouncementController::class,'show'])->name('announcement.show');
Route::post('/announcement/store/',[AnnouncementController::class, 'store'])->name('announcement.store');

//rotta filtri
Route::get('/category/{category}/announcements/', [PublicController::class, 'announcementsFilterByCategoryF'])->name('announcementsFilterByCategory');

Route::get('/announcement/show/{announcement}', [AnnouncementController::class, 'show'])->name('announcement.show');

