<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {   
        
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
            'phone' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],            
            'img' => ['required', 'image'],
            'password' => $this->passwordRules(),
        ])->validate();


            

        return User::create([
            
            'name' => $input['name'],
            'surname'=>$input['surname'],
            'phone'=>$input['phone'],
            'address'=>$input['address'],
            'email' => $input['email'],
            'img' => $input['img']->store('public/img/user'),
            'password' => Hash::make($input['password']),
            
        ]);
    }
}
