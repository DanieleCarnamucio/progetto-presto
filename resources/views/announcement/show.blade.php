<x-layout>
   <div class="container mt-5">
       <div class="row justify-content-center">

           
           <div class="col-6 d-flex justify-content-center">
           

            <div class="card" style="width: 18rem;">
                <div>DETTAGLIO DI: {{$announcement->title}}</div>
                <img class="card-img-top" src="https://picsum.photos/200" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">{{$announcement->price}}</h5>
                  <p class="card-text">{{$announcement->body}}</p>
                  <a href="{{route('home')}}" class="btn btn-primary">Torna alla home</a>
                </div>
              </div>
           </div>
           
        </div>
    </div> 
</x-layout>