<x-layout>
   
    
        <main class="form-signin">
        @if ($errors->any())
            @foreach ($errors->all() as $error)
            <img src="https://thumbs.gfycat.com/VeneratedFlashyAnt.webp" alt="">
            @endforeach
          @endif

          <form method="POST" action="{{route('login')}}">
            @csrf
           
            <h1 class="h3 mb-3 fw-normal">Accedi</h1>
        
            <div class="form-floating">
                <input name="email" type="email" class="form-control" id="femail" placeholder="Email" required>
                <label for="femail">Indirizzo email</label>
            </div>
            
           <div class="form-floating">
              <input name="password" type="password" class="form-control" id="fpassword" placeholder="Password" required>
              <label for="fpassword">Password</label>
            </div>
            
        
            <button class="w-100 btn btn-custom btn-lg btn-primary mb-1" type="submit">Accedi</button>
            <a class="w-100 btn btn-custom btn-lg btn-primary mb-1" href="{{route('register')}}">Registrati</a>
            <a class="w-100 btn btn-custom btn-lg btn-primary mb-1" href="{{route('home')}}">Home</a>
            
            <p class="mt-5 mb-3 text-muted">&copy; Bug Fiction H-34 2021</p>
          </form>

        </main>
        
        
            
          
</x-layout>